/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.neel.www.clientgcmdemo;

import android.content.Context;
import android.content.Intent;

public class QuickstartPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
  static final String SERVER_URL = "http://192.168.1.128/gcm/gcm.php?shareRegId=true";
   // static final String SERVER_URL = "http://192.168.1.10/gcm_serverp/register.php?shareRegId=true";

   // static final String SERVER_URL = "http://192.168.1.6/GcmFolder/register_user.php";


    public static final String Sender_id = "697361595086";
    static final String DISPLAY_MESSAGE_ACTION = "com.neel.www.clientgcmdemo.DISPLAY_MESSAGE";

    static final String EXTRA_MESSAGE = "message";

    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

}
